## Jekyll Static URL Shortener & Forwarder

From `http://short.domain.com/bar` -> `https://example.com/abc-is-this`  
From `http://short.domain.com/bar` -> `http://short.domain.com/bar` -> `https://example.com/abc-is-this`

### Definations:

* Short URL: `http://short.domain.com/bar`
* Destination URL: `https://example.com/abc-is-this`
* URL: `http://short.domain.com\`
* Base-URL: `/s` in `http://domain.com/s/bar` _optional_
* Slug: `/bar` _same as `title`_

### How to Use

#### One Time Setup:

1. Define url & baseurl in _config.yml

#### Everytime a new short URL is required:

1. Create a New File in `_posts` fodler with:
    * name: YYYY-MM-DD-title.md
        *  YYYY-MM-DD should be today's, as to note down the short URL's creation date.
        *  `title` will be the part `bar` in `http://short.domain.com/bar`
    * Sample YAML matter as:

```
---
redirect:
    to: "https://example.com/abc-is-this"
---
```

 1. Let gitlab build the jekyll build & pages.
 2. Done..


-------

Heavily Inspired by _Jekyll Plugin_ [Jekyll Redirect From](https://github.com/jekyll/jekyll-redirect-from)
Based / Forked from [Jekyll](https://jekyllrb.com/)
