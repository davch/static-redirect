---
layout: content
---
<ul>
{% for item in site.data.social %}{% if item[1].show %}
<li><a href="{{ item[1].href }}" target="_blank" class="contact_list" title="{{ item[1].title }} - {{ item[1].user }}"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-{{ item[1].icon }} fa-stack-1x fa-inverse"></i></span> {{ item[1].text }} - {{ item[1].user }}</a></li>{% endif %}
{% endfor %}
</ul>
Just a test for pages